﻿namespace BanksySan.MongoDbDemo.WebService
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;

    [BsonIgnoreExtraElements]
    internal class Restaurant
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public string Name { get; set; }
        public string Cuisine { get; set; }
    }
}