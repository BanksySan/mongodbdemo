﻿namespace BanksySan.MongoDbDemo.WebService
{
    using System;
    using MongoDB.Bson.Serialization;
    using Nancy.Hosting.Self;

    internal class Program
    {
        private const string HOST_URI = "http://localhost:8080";

        private static void Main(string[] args)
        {
            BsonClassMap.RegisterClassMap<Restaurant>(map =>
            {
                map.SetIgnoreExtraElements(true);
                map.MapProperty(x => x.Name).SetElementName("name");
                map.MapProperty(x => x.Cuisine).SetElementName("cuisine");
                map.MapProperty(x => x.Id).SetElementName("_id");
            });

            using (var server = new NancyHost(new Uri(HOST_URI)))
            {
                server.Start();
                Console.WriteLine("Listening on {0}", HOST_URI);
                Console.ReadLine();
            }
        }
    }
}