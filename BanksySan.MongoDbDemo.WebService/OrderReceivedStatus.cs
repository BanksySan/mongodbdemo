namespace BanksySan.MongoDbDemo.WebService
{
    internal abstract class OrderReceivedStatus
    {
        protected OrderReceivedStatus(string code)
        {
            Code = code;
        }

        public string Code { get; private set; }
    }
}