﻿namespace BanksySan.MongoDbDemo.WebService
{
    using System;

    internal class Order
    {
        public string RestaurantId { get; set; }

        public dynamic OrderDetails { get; set; }

        public DateTime ReceivedDate { get; set; }

        public OrderReceivedStatus Status { get; set; }
    }
}