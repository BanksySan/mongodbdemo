namespace BanksySan.MongoDbDemo.WebService
{
    internal class Burger
    {
        public string Name { get; set; }
        public bool SuperSize { get; set; }
    }
}