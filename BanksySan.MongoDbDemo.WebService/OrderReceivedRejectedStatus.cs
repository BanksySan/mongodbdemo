namespace BanksySan.MongoDbDemo.WebService
{
    internal class OrderReceivedRejectedStatus : OrderReceivedStatus
    {
        public OrderReceivedRejectedStatus() : base("rejected")
        {}
    }
}