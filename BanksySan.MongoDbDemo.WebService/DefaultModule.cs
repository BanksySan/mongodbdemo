﻿namespace BanksySan.MongoDbDemo.WebService
{
    using System.Collections.Generic;
    using System.IO;
    using MongoDB.Bson;
    using MongoDB.Bson.IO;
    using MongoDB.Driver;
    using Nancy;
    using JsonConvert = Newtonsoft.Json.JsonConvert;

    public class DefaultModule : NancyModule
    {
        private readonly IMongoDatabase _database;

        public DefaultModule()
        {
            var client =
                new MongoClient(new MongoClientSettings
                                {
                                    Servers =
                                        new[]
                                        {
                                            new MongoServerAddress("localhost", 27000),
                                            new MongoServerAddress("localhost", 27001),
                                            new MongoServerAddress("localhost", 27002)
                                        }
                                });

            _database = client.GetDatabase("restaurants");

            Get["/"] = p => "Hello World!";

            Get["/orders"] = p =>
            {
                var collection = _database.GetCollection<BsonDocument>("orders");

                var documents = collection.Find(new BsonDocument()).ToList();

                return documents.ToJson(new JsonWriterSettings {OutputMode = JsonOutputMode.Strict});
            };

            Get["/restaurants/start-{start}-count-{count}"] = p =>
                                                              {
                                                                  var nameEmptyFilter =
                                                                      Builders<Restaurant>.Filter.Eq(
                                                                          restaurant => restaurant.Name,
                                                                          string.Empty);
                                                                  var allFilter =
                                                                      Builders<Restaurant>.Filter.Not(nameEmptyFilter);

                                                                  int start = p["start"];
                                                                  int count = p["count"];

                                                                  var sort =
                                                                      new SortDefinitionBuilder<Restaurant>().Ascending(
                                                                          new StringFieldDefinition<Restaurant>("name"));
                                                                  var projection =
                                                                      Builders<Restaurant>.Projection.Include("name");

                                                                  var collection =
                                                                      _database.GetCollection<Restaurant>("grades");

                                                                  var cursor =
                                                                      collection.Find(allFilter)
                                                                                .Skip(start)
                                                                                .Limit(count)
                                                                                .Sort(sort)
                                                                                .Project(projection)
                                                                                .ToCursor();

                                                                  var documents = new List<BsonDocument>();

                                                                  while (cursor.MoveNext())
                                                                  {
                                                                      var batch = cursor.Current;

                                                                      documents.AddRange(batch);
                                                                  }

                                                                  return JsonConvert.SerializeObject(documents);
                                                              };

            Get["/restaurant/orders"] = p =>
                                        {
                                            var collection = _database.GetCollection<BsonDocument>("orders");

                                            var orders = collection.Find(new BsonDocument()).ToList();

                                            return
                                                orders.ToJson(new JsonWriterSettings
                                                              {
                                                                  OutputMode =
                                                                      JsonOutputMode.Strict
                                                              });
                                        };

            Post["/restaurant/{restaurantId}/order"] = p =>
                                                       {
                                                           ObjectId restaurantId;
                                                           string requestedRestaurantId = p["restaurantId"];
                                                           string orderBody;

                                                           using (var reader = new StreamReader(Request.Body))
                                                           {
                                                               orderBody = reader.ReadToEnd();
                                                           }

                                                           if (
                                                               !ObjectId.TryParse(requestedRestaurantId,
                                                                   out restaurantId))
                                                           {
                                                               return new Response
                                                                      {
                                                                          StatusCode =
                                                                              HttpStatusCode.BadRequest
                                                                      };
                                                           }

                                                           var restaurantsCollection =
                                                               _database.GetCollection<Restaurant>("grades");

                                                           var findRestaurantFilter =
                                                               Builders<Restaurant>.Filter.Eq("_id", restaurantId);
                                                           var restaurant =
                                                               restaurantsCollection.Find(findRestaurantFilter)
                                                                                    .Limit(1)
                                                                                    .SingleOrDefault();

                                                           if (restaurant == null)
                                                           {
                                                               return new NotFoundResponse();
                                                           }

                                                           if (restaurant.Cuisine.Contains("American"))
                                                           {
                                                               var americanCuisineOrder =
                                                                   JsonConvert.DeserializeObject<AmericanCuisineOrder>(
                                                                       orderBody);

                                                               var collection =
                                                                   _database.GetCollection<AmericanCuisineOrder>(
                                                                       "orders");
                                                               collection.InsertOne(americanCuisineOrder);
                                                           }
                                                           else
                                                           {
                                                               var defaultOrder =
                                                                   JsonConvert.DeserializeObject<DefaultCuisineOrder>(
                                                                       orderBody);
                                                               var collection =
                                                                   _database.GetCollection<DefaultCuisineOrder>("orders");
                                                               collection.InsertOne(defaultOrder);
                                                           }

                                                           return new Response { StatusCode = HttpStatusCode.OK };
                                                       };
        }
    }
}