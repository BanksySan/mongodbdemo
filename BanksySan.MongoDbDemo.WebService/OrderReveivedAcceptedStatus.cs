namespace BanksySan.MongoDbDemo.WebService
{
    internal class OrderReveivedAcceptedStatus : OrderReceivedStatus
    {
        public OrderReveivedAcceptedStatus() : base("accepted")
        {}
    }
}