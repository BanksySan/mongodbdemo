﻿namespace BanksySan.MongoDbDemo.BasicOperations
{
    using System;
    using System.Collections.Generic;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    public class SearchingTests
    {
        [SetUp]
        public void SetUp()
        {
            var client = new TestMongoClient();

            var database = client.GetDatabase("restaurants");

            _collection = database.GetCollection<BsonDocument>("grades");
        }

        private IMongoCollection<BsonDocument> _collection;

        [Test]
        public void AndSearch()
        {
            var builder = Builders<BsonDocument>.Filter;

            var filter = builder.Regex("cuisine", new BsonRegularExpression("Pakistani")) &
                         builder.Eq("borough", "Brooklyn");

            var result = _collection.Find(filter).ToList();

            Assert.That(result.Count, Is.GreaterThan(0));
        }

        [Test]
        public void ArrayContainsTests()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("grades.grade", "A");

            var documents = _collection.Find(filter).ToList();

            Assert.That(documents, Is.Not.Empty);
        }

        [Test]
        public void GetAll()
        {
            var filter = new BsonDocument();

            var result = _collection.Find(filter).ToList();

            Assert.That(result, Has.Count.GreaterThan(0));
        }

        [Test]
        public void GradeCountGreaterThanTwo()
        {
            var filter = Builders<BsonDocument>.Filter.SizeLt("grades", 2);

            var result = _collection.Find(filter).ToList();

            Assert.That(result.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GetAllDocumentsWithoutAnyGrades()
        {
            var existsFilter = Builders<BsonDocument>.Filter.Exists(x => x["grades"]);
            var filter = Builders<BsonDocument>.Filter.Not(existsFilter);

            var documents = _collection.Find(filter).ToList();

            foreach (var document in documents)
            {
                Assert.Throws<KeyNotFoundException>(() => { var x = document["grades"]; });
            }
        }

        [Test]
        public void GetRestaurantsWithOnlyScoresLessThan()
        {
            var builder = Builders<BsonDocument>.Filter;

            var scoreFilter = builder.Gt(x => x["grades.score"], 20);
            var projection = Builders<BsonDocument>.Projection.Include(x => x["grades"]);

            var documents =
                _collection.Find(Builders<BsonDocument>.Filter.Not(scoreFilter)).Project(projection).ToList();

            foreach (var document in documents)
            {
                try
                {
                    foreach (var grade in document["grades"].AsBsonArray)
                    {
                        var score = grade["score"];

                        if (!score.IsBsonNull)
                        {
                            Assert.That(score.AsInt32, Is.LessThan(21));
                        }
                    }
                }
                catch (KeyNotFoundException)
                {
                    // If there's no grades element then there's grades.
                }
            }
        }

        [Test]
        public void GetLatestGrade()
        {
            var filter = Builders<BsonDocument>.Filter.Eq(document => document["borough"], "Bronx");
            var projection =
                Builders<BsonDocument>.Projection.Include(document => document["name"])
                                      .Include(document => document["grades"]);

            var documents = _collection.Aggregate().Match(filter).Project(projection)
                       .Unwind(document => document["grades"])
                       .SortByDescending(document => document["grades.date"])
                       .ToList();

            foreach (var document in documents)
            {
                // Since we're unwinding, 'grades' isn't an array any more.  
                // Unwind converts it to a standard BSON document
                Assert.That(document["grades"], Is.AssignableTo<BsonDocument>());
            }
        }

        [Test]
        public void ElemMatch()
        {
            var gradeFilterBuilder = Builders<BsonDocument>.Filter;

            var scoreFilter = gradeFilterBuilder.Lte(document => document["score"], 7);
            var gradeFilter = gradeFilterBuilder.Eq(document => document["grade"], "A");

            var projection = Builders<BsonDocument>.Projection.Include("grades").Include("name");

            var filter = Builders<BsonDocument>.Filter.ElemMatch("grades", scoreFilter & gradeFilter);

            var results = _collection.Find(filter).Project(projection).ToList();

            foreach (var result in results)
            {
                var matched = false;

                foreach (var grade in result["grades"].AsBsonArray)
                {
                    if (grade["score"].AsInt32 <= 7 && grade["grade"].AsString == "A")
                    {
                        matched = true;
                        break;
                    }
                }

                Assert.That(matched, Is.True);
            }
        }
    }
}