﻿namespace BanksySan.MongoDbDemo.BasicOperations
{
    using System.Collections.Generic;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class ProjectionTests
    {
        [SetUp]
        public void SetUp()
        {
            var client = new TestMongoClient();

            var database = client.GetDatabase("restaurants");

            _collection = database.GetCollection<BsonDocument>("grades");
        }

        private IMongoCollection<BsonDocument> _collection;

        [Test]
        public void ExcludeAddress()
        {
            var projection = Builders<BsonDocument>.Projection.Exclude("address");
            var gradesExistsFilter = Builders<BsonDocument>.Filter.Exists(document => document["grades"]);
            var documents = _collection.Find(gradesExistsFilter).Project(projection);

            using (var cursor = documents.ToCursor())
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;

                    foreach (var document in batch)
                    {
                        Assert.That(document["borough"], Is.Not.Null);
                        Assert.That(document["cuisine"], Is.Not.Null);
                        Assert.That(document["name"], Is.Not.Null);
                        Assert.That(document["grades"], Is.Not.Null);
                        Assert.That(document["restaurant_id"], Is.Not.Null);

                        Assert.Throws<KeyNotFoundException>(() => { var x = document["address"]; });
                    }
                }
            }
        }

        [Test]
        public void GetAllAddresses()
        {
            var projection = Builders<BsonDocument>.Projection.Include("address");
            var gradesExistsFilter = Builders<BsonDocument>.Filter.Exists(document => document["grades"]);

            var documents = _collection.Find(gradesExistsFilter).Project(projection);

            using (var cursor = documents.ToCursor())
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;

                    foreach (var document in batch)
                    {
                        Assert.Throws<KeyNotFoundException>(() => { var x = document["borough"]; });
                        Assert.Throws<KeyNotFoundException>(() => { var x = document["cuisine"]; });
                        Assert.Throws<KeyNotFoundException>(() => { var x = document["name"]; });
                        Assert.Throws<KeyNotFoundException>(() => { var x = document["grades"]; });
                        Assert.Throws<KeyNotFoundException>(() => { var x = document["restaurant_id"]; });

                        var address = document["address"];
                        Assert.That(address["building"], Is.Not.Null);
                        Assert.That(address["coord"], Is.Not.Null);
                        Assert.That(address["street"], Is.Not.Null);
                        Assert.That(address["zipcode"], Is.Not.Null);
                    }
                }
            }
        }
    }
}