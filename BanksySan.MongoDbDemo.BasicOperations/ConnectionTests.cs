﻿namespace BanksySan.MongoDbDemo.BasicOperations
{
    using System;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class ConnectionTests
    {
        [Test]
        public void ConnectToReplicationSet()
        {
            var client =
                new MongoClient(new MongoClientSettings
                {
                    Servers =
                        new[]
                        {
                            new MongoServerAddress("localhost", 27000),
                            new MongoServerAddress("localhost", 27001),
                            new MongoServerAddress("localhost", 27002)
                        }
                });

            var database = client.GetDatabase("connectionTests");

            var collection = database.GetCollection<BsonDocument>("default");

            var document = new BsonDocument {{"message", "Hello World!"}, {"created-date", DateTime.Now}};

            collection.InsertOne(document);

            Assert.That(document["_id"], Is.Not.Null);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", document["_id"]);

            var retrievedDocument = collection.Find(filter).Single();

            Assert.That(retrievedDocument["_id"], Is.EqualTo(document["_id"]));
            Assert.That(retrievedDocument["message"], Is.EqualTo(document["message"]));
            Assert.That(retrievedDocument["created-date"], Is.EqualTo(document["created-date"]));
        }

        [Test]
        public void ConnectToSingleInstance()
        {
            var client = new TestMongoClient();

            var database = client.GetDatabase("connectionTests");

            var collection = database.GetCollection<BsonDocument>("default");

            var document = new BsonDocument {{"message", "Hello World!"}, {"created-date", DateTime.Now}};

            collection.InsertOne(document);

            Assert.That(document["_id"], Is.Not.Null);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", document["_id"]);

            var retrievedDocument = collection.Find(filter).Single();

            Assert.That(retrievedDocument["_id"], Is.EqualTo(document["_id"]));
            Assert.That(retrievedDocument["message"], Is.EqualTo(document["message"]));
            Assert.That(retrievedDocument["created-date"], Is.EqualTo(document["created-date"]));
        }
    }
}