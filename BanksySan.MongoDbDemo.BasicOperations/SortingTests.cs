﻿namespace BanksySan.MongoDbDemo.BasicOperations
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class SortingTests
    {
        [SetUp]
        public void SetUp()
        {
            var client = new TestMongoClient();

            var database = client.GetDatabase("restaurants");

            _collection = database.GetCollection<BsonDocument>("grades");
        }

        private IMongoCollection<BsonDocument> _collection;

        [Test]
        public void SortByName()
        {
            var sort =
                new SortDefinitionBuilder<BsonDocument>().Ascending(new StringFieldDefinition<BsonDocument>("name"));
            var projection = Builders<BsonDocument>.Projection.Include("name");

            var documents = _collection.Find(new BsonDocument()).Sort(sort).Project(projection).ToList();

            Assert.That(documents, Has.Count.GreaterThan(0));
        }
    }
}