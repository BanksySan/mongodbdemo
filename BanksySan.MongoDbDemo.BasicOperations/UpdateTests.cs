﻿namespace BanksySan.MongoDbDemo.BasicOperations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class UpdateTests
    {
        [SetUp]
        public void SetUp()
        {
            var client = new TestMongoClient();

            var database = client.GetDatabase("restaurants");

            _collection = database.GetCollection<BsonDocument>("grades");
        }

        private IMongoCollection<BsonDocument> _collection;

        [Test]
        public void AddNewField()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId("56bf7957b5e096fd06b755b0"));

            var document = _collection.Find(filter).Single();

            var revertUpdate = Builders<BsonDocument>.Update.Unset("foo");

            _collection.UpdateOne(filter, revertUpdate);

            Assert.Throws<KeyNotFoundException>(() => { var bar = document["foo"]; });

            var update = Builders<BsonDocument>.Update.Set("foo", "bar");

            var updateResult = _collection.UpdateOne(filter, update);

            Assert.That(updateResult.ModifiedCount, Is.EqualTo(1));

            var updatedDocument = _collection.Find(filter).Single();

            Assert.That(updatedDocument["foo"].AsString, Is.EqualTo("bar"));

            _collection.UpdateOne(filter, revertUpdate);

            Assert.Throws<KeyNotFoundException>(() => { var bar = document["foo"]; });
        }

        [Test]
        public void AddNewGrade()
        {
            var newGrade = new BsonDocument {{"date", DateTime.Now}, {"grade", "A*"}, {"score", 10}};

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId("56bf7957b5e096fd06b755b0"));

            var originalDocument = _collection.Find(filter).Single();
            var originalGrades = originalDocument["grades"].AsBsonArray;

            var update = Builders<BsonDocument>.Update.AddToSet("grades", newGrade);
            _collection.UpdateOne(filter, update);

            var document = _collection.Find(filter).Single();

            var grades = document["grades"].AsBsonArray;

            Assert.That(grades.Count, Is.EqualTo(originalGrades.Count + 1));
        }

        [Test]
        public void RemoveGrade()
        {
            var code = Guid.NewGuid().ToString();

            var newGrade = new BsonDocument
                           {
                               { "date", DateTime.Now },
                               { "grade", "A*" },
                               { "score", 10 },
                               { "uniqueCode", code }
                           };

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId("56bf7957b5e096fd06b755b0"));

            _collection.UpdateOne(filter, Builders<BsonDocument>.Update.AddToSet("grades", newGrade));

            var gradeFilter = Builders<BsonDocument>.Filter.Eq(x => x["uniqueCode"], code);

            var pullUpdate = Builders<BsonDocument>.Update.PullFilter("grades", gradeFilter);

            _collection.UpdateOne(filter, pullUpdate);

            var newDocument = _collection.Find(filter).Single();

            foreach (var grade in newDocument["grades"].AsBsonArray)
            {
                Assert.Throws<KeyNotFoundException>(() => { var x = grade["uniqueCode"]; });
            }
        }
    }
}